###########################
### FRAGMENTS PRACTICAL ###
###########################

## STEP 1 - Prerequesites ##
# Install Anaconda (Can be done in virtual machine)
# Install conda environment:
conda env create -f fragenv.yml
# Activate the enviroment:
source activate fragenv

Start Jupyter Notebook in the Anaconda environment. 

## STEP 2 - Building Fragments ##

Run the Jupyter notebook: “build_fragment.ipynb”. This takes a fragment bound to a bromodomain (PDB code: 5enc, Elliot look at the electron density). The fragment is then broken into building blocks using retrosynthesis. These building blocks are then grown using common chemical synthesis reactions to form a library of candidate compounds. 

## STEP 3 - Scoring Fragments ##

Run the Jupiter notebook: “score_fragment.ipynb”. This takes the library of candidate compounds and generates conformers using RDKit. These are then scored using SMINA (a fork of AutoDock Vina). The output will be in “smina_docking” and there will be a log file with the corresponding scores for each conformation and a PDBQT file with the conformations. 

*********************************************
## STEP 4 - CHALLENGE ##

You are a highly-sought after, experienced, intelligent computational chemist. A crystallographer has kindly given you a PDB structure of a target with a fragment hit (PDB code: 5CQA). Your challenge is to present to us (synthetic chemists) at least one follow-up compound with reasons why we should make it for you. 